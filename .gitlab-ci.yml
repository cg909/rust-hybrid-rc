stages:
  - build
  - test
  - pages
  - deploy

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .cargo/
    - target/criterion/
  policy: pull-push

variables:
  CARGO_INCREMENTAL: 0
  CARGO_HOME: ${CI_PROJECT_DIR}/.cargo
  CLIPPY_OPTS: -W clippy::pedantic -A clippy::must_use_candidate -A clippy::tabs_in_doc_comments -A clippy::inline_always -A unused_unsafe

default:
  image: rust:buster

build:stable:
  stage: build
  script:
    - cargo build --workspace --all-features --color=always
  artifacts:
    public: false
    paths:
      - target/debug/
    expire_in: 1 day

test:stable:
  stage: test
  needs:
    - build:stable
  dependencies:
    - build:stable
  before_script:
    - 'echo "deb http://deb.debian.org/debian/ sid main contrib" >> /etc/apt/sources.list'
    - apt-get update -qy
    - apt-get -qy install kcov
    - wget https://grenz-bonn.de/rust2junit.py
    - chmod +x rust2junit.py
  script:
    - export RUSTFLAGS="-C link-dead-code -C inline-threshold=0"
    - cargo check --workspace --color=always
    - cargo test --workspace --no-default-features --verbose
    - rm target/debug/deps/hybrid_rc-????????????????
    - cargo test --workspace --verbose | ./rust2junit.py
    - kcov --verify
        --include-pattern=/rust-hybrid-rc/src/
        --exclude-line='#[derive,pub struct'
        --exclude-region='#[cfg(test)]:#[cfg(testkcovstopmarker)]'
        --exclude-pattern=tests.rs
        target/cov
        target/debug/deps/hybrid_rc-???????????????? --test-threads 1
    - cp target/cov/hybrid_rc-????????????????.*/cobertura.xml target/cov/
    - COVERAGE=$(grep -Po 'covered":.*?[^\\]"' target/cov/index.js | grep "[0-9]*\.[0-9]" -o)
    - echo "Coverage:" $COVERAGE
  artifacts:
    public: false
    paths:
      - target/cov/
    expire_in: 1 week
    reports:
      junit: junit.xml
      cobertura: target/cov/cobertura.xml

clippy:
  stage: test
  before_script:
    - rustup component add clippy
    - cargo install gitlab_clippy
  script:
    - cargo clippy -- ${CLIPPY_OPTS}
  after_script:
    - 'cargo clippy --message-format=json -- ${CLIPPY_OPTS} | ${CARGO_HOME}/bin/gitlab-clippy > gl-code-quality-report.json'
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    expire_in: 1 week
  only:
    - master
  needs: []

benchmark:
  stage: test
  before_script:
    - cargo install cargo-criterion
  script:
    - cargo criterion
  artifacts:
    public: false
    paths:
      - target/criterion/
    expire_in: 1 week
  only:
    - master
  needs: []

pages:
  stage: pages
  only:
    - master
  needs:
    - test:stable
    - benchmark
  dependencies:
    - test:stable
    - benchmark
  script:
    - cargo doc --workspace --all-features --color=always
    - rm -rf public || true
    - mkdir public
    - cp -R target/doc/* public
    - cp -R target/cov public/
    - cp -R target/criterion public/
    - find -L public/cov/ -type l -delete
  artifacts:
    paths:
      - public

publish:
  stage: deploy
  dependencies: []
  when: manual
  needs:
    - "test:stable"
  only:
    refs:
      - tags
    variables:
      - $CI_COMMIT_TAG =~ /^v[0-9]/
  script:
    - cargo publish --no-verify
